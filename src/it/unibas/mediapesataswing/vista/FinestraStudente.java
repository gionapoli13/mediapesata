package it.unibas.mediapesataswing.vista;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.controllo.Controllo;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FinestraStudente extends JDialog {
    
    private Vista vista;
    private Controllo controllo;
    private Modello modello;
    
    public FinestraStudente(Vista vista) {
        super(vista);
        this.vista = vista;
        this.controllo = vista.getControllo();
        this.modello = vista.getModello();
        this.setModal(true);
        this.inizializza();
    }
    
    private JTextField campoCognome = new JTextField();
    private JTextField campoMatricola = new JTextField();
    private JTextField campoNome = new JTextField();
    
    public String getCampoNome() {
        return this.campoNome.getText();
    }
    
    public String getCampoCognome() {
        return this.campoCognome.getText();
    }
    
    public String getCampoMatricola() {
        return this.campoMatricola.getText();
    }
    
    public void inizializza() {
        this.setTitle("Inserisci i Dati dello Studente");
        creaPannelloAggiornaStudente();
        this.pack();        
    }
    
    private void creaPannelloAggiornaStudente() {
        JPanel pannelloStudente = new JPanel();
        JLabel labelNome = new JLabel("Nome Studente");
        campoNome.setColumns(10);
        pannelloStudente.add(labelNome);
        pannelloStudente.add(campoNome);
        JLabel labelCognome = new JLabel("Cognome studente");
        campoCognome.setColumns(10);
        pannelloStudente.add(labelCognome);
        pannelloStudente.add(campoCognome);
        JLabel labelMatricola = new JLabel("Matricola");
        campoMatricola.setColumns(5);
        pannelloStudente.add(labelMatricola);
        pannelloStudente.add(campoMatricola);
        Action azioneAggiornaStudente = this.controllo.getAzione(Costanti.AZIONE_AGGIORNA_STUDENTE);
        JButton bottoneAggiornaStudente = new JButton(azioneAggiornaStudente);
        campoNome.setAction(azioneAggiornaStudente);
        campoCognome.setAction(azioneAggiornaStudente);
        campoMatricola.setAction(azioneAggiornaStudente);
        pannelloStudente.add(bottoneAggiornaStudente);
        this.getContentPane().add(pannelloStudente);
    }
    
    public void visualizza() {
        Modello modello = this.controllo.getModello();
        Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
        this.campoNome.setText(studente.getNome());
        this.campoCognome.setText(studente.getCognome());
        this.campoMatricola.setText(studente.getMatricola() + "");
        this.setVisible(true);
    }
    
    public void nascondi() {
        this.setVisible(false);
    }
}
