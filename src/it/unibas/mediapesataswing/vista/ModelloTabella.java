package it.unibas.mediapesataswing.vista;

import it.unibas.mediapesataswing.modello.Studente;

public class ModelloTabella extends javax.swing.table.AbstractTableModel {
    
    private Studente studente;
   
    public ModelloTabella (Studente studente) {
        this.studente = studente;
    }
    
    public Object getValueAt(int x, int y) {
        if (y == 0) {
            return studente.getEsame(x).getInsegnamento();
        } else if (y == 1) {
            return studente.getEsame(x).getCrediti();
        } else if (y == 2) {
            return studente.getEsame(x).getVoto();
        } else if (y == 3) {
            return studente.getEsame(x).isLode();
        }
        return null;
    }

    public int getRowCount() {
        return studente.getNumeroEsami();
    }

    public String getColumnName(int i) {
        if (i == 0) {
            return "Insegnamento";
        } else if (i == 1) {
            return "Crediti";
        } else if (i == 2) {
            return "Voto";
        } else if (i == 3) {
            return "Lode";
        }   
        return null;
    }

    public int getColumnCount() {
        return 4;
    }

    public Class getColumnClass(int i) {
        if (i == 3) {
            return Boolean.class;
        }
        return super.getColumnClass(i);
    }
}
