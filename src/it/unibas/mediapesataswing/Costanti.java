package it.unibas.mediapesataswing;

import it.unibas.mediapesataswing.controllo.AzioneModificaEsame;
import it.unibas.mediapesataswing.controllo.AzioneModificaStudente;
import it.unibas.mediapesataswing.controllo.AzioneApri;
import it.unibas.mediapesataswing.controllo.AzioneApriExcel;
import it.unibas.mediapesataswing.controllo.AzioneApriExcelNoThread;
import it.unibas.mediapesataswing.controllo.AzioneCalcolaMedia;
import it.unibas.mediapesataswing.controllo.AzioneEliminaEsame;
import it.unibas.mediapesataswing.controllo.AzioneEsci;
import it.unibas.mediapesataswing.controllo.AzioneFinestraInformazioni;
import it.unibas.mediapesataswing.controllo.AzioneInserisciEsame;
import it.unibas.mediapesataswing.controllo.AzioneFinestraModificaEsame;
import it.unibas.mediapesataswing.controllo.AzioneFinestraModificaStudente;
import it.unibas.mediapesataswing.controllo.AzioneNuovo;
import it.unibas.mediapesataswing.controllo.AzioneSalva;
import it.unibas.mediapesataswing.controllo.AzioneVisualizzaLista;
import it.unibas.mediapesataswing.controllo.AzioneVisualizzaTabella;
import it.unibas.mediapesataswing.modello.Esame;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.vista.FinestraEsame;
import it.unibas.mediapesataswing.vista.FinestraStudente;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;

public class Costanti {
    
    public static final String AZIONE_AGGIORNA_ESAME = AzioneModificaEsame.class.getName();
    public static final String AZIONE_AGGIORNA_STUDENTE = AzioneModificaStudente.class.getName();
    public static final String AZIONE_APRI = AzioneApri.class.getName();
    public static final String AZIONE_APRI_EXCEL = AzioneApriExcel.class.getName();
    public static final String AZIONE_APRI_EXCEL_NO_THREAD = AzioneApriExcelNoThread.class.getName();
    public static final String AZIONE_CALCOLA_MEDIA = AzioneCalcolaMedia.class.getName();
    public static final String AZIONE_ELIMINA_ESAME = AzioneEliminaEsame.class.getName();
    public static final String AZIONE_ESCI = AzioneEsci.class.getName();
    public static final String AZIONE_INFORMAZIONI = AzioneFinestraInformazioni.class.getName();
    public static final String AZIONE_INSERISCI_ESAME = AzioneInserisciEsame.class.getName();
    public static final String AZIONE_MODIFICA_ESAME = AzioneFinestraModificaEsame.class.getName();
    public static final String AZIONE_MODIFICA_STUDENTE = AzioneFinestraModificaStudente.class.getName();
    public static final String AZIONE_NUOVO = AzioneNuovo.class.getName();
    public static final String AZIONE_SALVA = AzioneSalva.class.getName();
    public static final String AZIONE_VISUALIZZA_LISTA = AzioneVisualizzaLista.class.getName();
    public static final String AZIONE_VISUALIZZA_TABELLA = AzioneVisualizzaTabella.class.getName();
    
    public static final String STUDENTE = Studente.class.getName();
    public static final String ESAME = Esame.class.getName();
            
    public static final String VISTA_FINESTRA_ESAME = FinestraEsame.class.getName();
    public static final String VISTA_FINESTRA_STUDENTE = FinestraStudente.class.getName();
    public static final String VISTA_PANNELLO_PRINCIPALE = PannelloPrincipale.class.getName();
    
    public static final String ESEGUIBILE_EXCEL = "C:\\Program Files\\Microsoft Office\\Office12\\Excel.exe";

}
