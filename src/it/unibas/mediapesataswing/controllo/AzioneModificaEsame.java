package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Esame;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.vista.FinestraEsame;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;

public class AzioneModificaEsame extends javax.swing.AbstractAction {

    private Controllo controllo;
    
    public AzioneModificaEsame(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Invia");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Modifica i dati dell'esame selezionato");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_E));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Modello modello = this.controllo.getModello();
        Vista vista = this.controllo.getVista();
        FinestraEsame finestra = (FinestraEsame)vista.getSottoVista(Costanti.VISTA_FINESTRA_ESAME);
        String campoInsegnamento = finestra.getCampoInsegnamento();
        String campoCrediti = finestra.getCampoCrediti();
        String campoVoto = finestra.getCampoVoto();
        boolean checkLode = finestra.getCheckLode();
        String errori = verifica(campoInsegnamento, campoCrediti, campoVoto, checkLode);
        if (errori.equals("")) {
            Esame esame = (Esame)modello.getBean(Costanti.ESAME);
            esame.setInsegnamento(campoInsegnamento);
            esame.setCrediti(Integer.parseInt(campoCrediti));
            esame.setVoto(Integer.parseInt(campoVoto));
            esame.setLode(checkLode);
            PannelloPrincipale pannelloPrincipale = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE);
            pannelloPrincipale.aggiornaEsami();
            finestra.nascondi();
        } else {
            this.controllo.getVista().finestraErrore(errori);
        }
    }
    
    private String verifica(String campoInsegnamento, String campoCrediti,
            String campoVoto, boolean checkLode) {
        String errori = "";
        if (campoInsegnamento.equals("") || campoInsegnamento == null) {
            errori += "L'insegnamento non pu� essere nullo \n";
        }
        try {
            int crediti = Integer.parseInt(campoCrediti);
            if (crediti < 0 || crediti > 30) {
                errori += "Errore nel valore dei crediti \n";
            }
        } catch (NumberFormatException nfe) {
            errori += "I crediti devono essere un numero \n";
        }
        try {
            int voto = Integer.parseInt(campoVoto);
            if (voto < 18 || voto > 30) {
                errori += "Errore nel valore del voto \n";
            }
        } catch (NumberFormatException nfe) {
            errori += "Il voto deve essere un numero \n";
        }
        if (errori.equals("")) {
            if (checkLode && Integer.parseInt(campoVoto) != 30) {
                errori += "La lode � possibile solo se il voto � 30 \n";
            }
        }
        return errori;
    }
    
}
