package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.persistenza.DAOException;
import it.unibas.mediapesataswing.persistenza.DAOStudente;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;
import java.io.File;
import javax.swing.JFileChooser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class AzioneApri extends javax.swing.AbstractAction {
    
    private Log logger = LogFactory.getLog(this.getClass());

    private Controllo controllo;
    
    public AzioneApri(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Apri");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Carica i dati dello studente dal disco");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_A));
        this.putValue(javax.swing.Action.ACCELERATOR_KEY, javax.swing.KeyStroke.getKeyStroke("ctrl O"));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        String nomeFile = acquisisciFile();
        if (nomeFile != null) {
            try {
                caricaDatiStudente(nomeFile);
                abilitaAzioni();
            } catch (DAOException daoe) {
                logger.error("Impossibile caricare il file: " + daoe);
                this.controllo.getVista().finestraErrore("Impossibile caricare il file " + daoe);
            }
        }
    }
    
    private String acquisisciFile() {
        JFileChooser fileChooser = this.controllo.getVista().getFileChooser();
        int codice = fileChooser.showOpenDialog(this.controllo.getVista());
        if (codice == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            return file.toString();
        } else if (codice == JFileChooser.CANCEL_OPTION) {
            logger.info("Comando apri annullato");
        }
        return null;
    }
    
    private void caricaDatiStudente(String nomeFile) throws DAOException {
        Studente studente = DAOStudente.carica(nomeFile);
        Modello modello = this.controllo.getModello();
        modello.putBean(Costanti.STUDENTE, studente);
        Vista vista = this.controllo.getVista();
        PannelloPrincipale pannello = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE);
        pannello.schermoStudente();
    }
    
    private void abilitaAzioni() {
        this.controllo.getAzione(Costanti.AZIONE_SALVA).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_INSERISCI_ESAME).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_MODIFICA_ESAME).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_ELIMINA_ESAME).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_MODIFICA_STUDENTE).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_CALCOLA_MEDIA).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_VISUALIZZA_TABELLA).setEnabled(true);        
        this.controllo.getAzione(Costanti.AZIONE_APRI_EXCEL).setEnabled(true);
        this.controllo.getAzione(Costanti.AZIONE_APRI_EXCEL_NO_THREAD).setEnabled(true);        
    }
}
