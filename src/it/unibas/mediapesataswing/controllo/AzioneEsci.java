package it.unibas.mediapesataswing.controllo;

import javax.swing.AbstractAction;

public class AzioneEsci extends AbstractAction {
    
    private Controllo controllo;
    
    public AzioneEsci(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Esci");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Esce dall'applicazione");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_E));
        this.putValue(javax.swing.Action.ACCELERATOR_KEY, javax.swing.KeyStroke.getKeyStroke("ctrl E"));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        System.exit(0);
    }
}
