package it.unibas.mediapesataswing.controllo;

import javax.swing.AbstractAction;

public class AzioneFinestraInformazioni extends AbstractAction {
    
    private Controllo controllo;
    
    public AzioneFinestraInformazioni(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Informazioni sull'Applicazione");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Informazioni sull'applicazione");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_M));
        this.putValue(javax.swing.Action.ACCELERATOR_KEY, javax.swing.KeyStroke.getKeyStroke("ctrl H"));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        this.controllo.getVista().finestraAbout();
    }
}
