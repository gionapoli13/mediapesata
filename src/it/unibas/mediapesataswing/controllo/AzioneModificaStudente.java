package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.vista.FinestraStudente;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;
import javax.swing.AbstractAction;

public class AzioneModificaStudente extends AbstractAction {

    private Controllo controllo;
    
    public AzioneModificaStudente(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Invia");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Modifica i Dati dello Studente");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_M));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Vista vista = this.controllo.getVista();
        FinestraStudente finestra = (FinestraStudente)vista.getSottoVista(Costanti.VISTA_FINESTRA_STUDENTE);
        String campoNome = finestra.getCampoNome();
        String campoCognome = finestra.getCampoCognome();
        String campoMatricola = finestra.getCampoMatricola();
        String errori = verifica(campoNome, campoCognome, campoMatricola);
        if (errori.equals("")) {
           Modello modello = this.controllo.getModello();
           Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
           studente.setNome(campoNome);
           studente.setCognome(campoCognome);
           studente.setMatricola(Integer.parseInt(campoMatricola));
           FinestraStudente finestraStudente = (FinestraStudente)vista.getSottoVista(Costanti.VISTA_FINESTRA_STUDENTE);
           finestraStudente.nascondi();
           PannelloPrincipale pannelloPrincipale = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE);
           pannelloPrincipale.schermoStudente();
        } else {
           this.controllo.getVista().finestraErrore(errori);
        }
    }
    
    public String verifica(String campoNome, String campoCognome, String campoMatricola) {
        String errori = "";
        if (campoNome.equals("") || campoNome == null) {
            errori += "Il nome non pu� essere nullo \n";
        }
        if (campoCognome.equals("") || campoCognome == null) {
            errori += "Il cognome non pu� essere nullo \n";
        }
        try {
            int matricola = Integer.parseInt(campoMatricola);
            if (matricola < 0) {
                errori += "La matricola deve essere un numero positivo";
            }
        } catch (NumberFormatException nfe) {
            errori += "La matricola deve essere un numero";
        }
        return errori;
    }

}
