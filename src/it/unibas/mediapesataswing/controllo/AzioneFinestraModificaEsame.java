package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Esame;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;

public class AzioneFinestraModificaEsame extends javax.swing.AbstractAction {
    
    private Controllo controllo;
    
    public AzioneFinestraModificaEsame(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Modifica Esame");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Modifica i dati dell'esame");
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Modello modello = this.controllo.getModello();
        Vista vista = this.controllo.getVista();
        PannelloPrincipale pannelloPrincipale = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE); 
        int indiceSelezione = pannelloPrincipale.getSelectedIndex();
        if (indiceSelezione != -1) {
            Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
            Esame esame = studente.getEsame(indiceSelezione);
            if (esame != null) {
                modello.putBean(Costanti.ESAME, esame);
                this.controllo.getVista().finestraModificaEsame();
            }
        } else {
            vista.finestraErrore("E' necessario selezionare un esame");
        }
    }
    
}
