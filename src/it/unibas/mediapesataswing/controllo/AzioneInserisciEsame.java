package it.unibas.mediapesataswing.controllo;

import it.unibas.mediapesataswing.Costanti;
import it.unibas.mediapesataswing.modello.Modello;
import it.unibas.mediapesataswing.modello.Studente;
import it.unibas.mediapesataswing.vista.PannelloPrincipale;
import it.unibas.mediapesataswing.vista.Vista;

public class AzioneInserisciEsame extends javax.swing.AbstractAction {
    
    private Controllo controllo;
    
    public AzioneInserisciEsame(Controllo controllo) {
        this.controllo = controllo;
        this.putValue(javax.swing.Action.NAME, "Inserisci Esame");
        this.putValue(javax.swing.Action.SHORT_DESCRIPTION, "Inserisce l'esame nella lista");
        this.putValue(javax.swing.Action.MNEMONIC_KEY, new Integer(java.awt.event.KeyEvent.VK_I));
    }
    
    public void actionPerformed(java.awt.event.ActionEvent e) {
        Vista vista = controllo.getVista();
        PannelloPrincipale pannello = (PannelloPrincipale)vista.getSottoVista(Costanti.VISTA_PANNELLO_PRINCIPALE);
        String campoInsegnamento = pannello.getCampoInsegnamento();
        String campoCrediti = pannello.getCampoCrediti();
        String campoVoto = pannello.getCampoVoto();
        boolean checkLode = pannello.getCheckLode();
        String errori = verifica(campoInsegnamento, campoCrediti, campoVoto, checkLode);
        if (errori.equals("")) {
            Modello modello = this.controllo.getModello();
            Studente studente = (Studente)modello.getBean(Costanti.STUDENTE);
            studente.addEsame(campoInsegnamento, Integer.parseInt(campoVoto), checkLode, Integer.parseInt(campoCrediti));
            this.controllo.getAzione(Costanti.AZIONE_CALCOLA_MEDIA).setEnabled(true);
            pannello.schermoStudente();
            pannello.ripulisciEsame();
        } else {
            this.controllo.getVista().finestraErrore(errori);
        }
    }
    
    private String verifica(String campoInsegnamento, String campoCrediti,
            String campoVoto, boolean checkLode) {
        String errori = "";
        if (campoInsegnamento.equals("") || campoInsegnamento == null) {
            errori += "L'insegnamento non pu� essere nullo \n";
        }
        try {
            int crediti = Integer.parseInt(campoCrediti);
            if (crediti < 0 || crediti > 30) {
                errori += "Errore nel valore dei crediti \n";
            }
        } catch (NumberFormatException nfe) {
            errori += "I crediti devono essere un numero \n";
        }
        try {
            int voto = Integer.parseInt(campoVoto);
            if (voto < 18 || voto > 30) {
                errori += "Errore nel valore del voto \n";
            }
        } catch (NumberFormatException nfe) {
            errori += "Il voto deve essere un numero \n";
        }
        if (errori.equals("")) {
            if (checkLode && Integer.parseInt(campoVoto) != 30) {
                errori += "La lode � possibile solo se il voto � 30 \n";
            }
        }
        return errori;
    }
    
}
