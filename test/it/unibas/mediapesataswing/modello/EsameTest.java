package it.unibas.mediapesataswing.modello;

import static org.junit.Assert.*;

import org.junit.Test;

public class EsameTest {

	@Test
	public void shouldReturnInsegnamento() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 25, false, 9);
		// assert
		assertEquals("Test", e.getInsegnamento());
	}

	@Test
	public void shouldReturnVoto() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 25, false, 9);
		// assert
		assertEquals(25, e.getVoto());
	}

	@Test
	public void shouldReturnCrediti() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 25, false, 9);
		// assert
		assertEquals(9, e.getCrediti());
	}

	@Test
	public void shouldReturnNoLode() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 25, false, 9);
		// assert
		assertFalse(e.isLode());
	}

	@Test
	public void shouldReturnLode() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 30, true, 9);
		// assert
		assertTrue(e.isLode());
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowException1() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 17, true, 9);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowException2() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 31, true, 9);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowException3() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 30, true, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void shouldThrowException4() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 29, true, 0);
	}

	@Test
	public void shouldreturnTostring() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 25, false, 9);
		// assert
		assertEquals("Esame di Test (9 CFU) - voto: 25", e.toString());
	}

	@Test
	public void shouldreturnToStringLode() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 30, true, 9);
		// assert
		assertEquals("Esame di Test (9 CFU) - voto: 30 e lode", e.toString());
	}

	@Test
	public void shouldreturnToSaveString() {
		// arrange
		Esame e;
		// act
		e = new Esame("Test", 25, false, 9);
		// assert
		assertEquals("Test , 9 , 25 , false", e.toSaveString());
	}

}
