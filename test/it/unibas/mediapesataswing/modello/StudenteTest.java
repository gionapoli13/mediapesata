package it.unibas.mediapesataswing.modello;

import static org.junit.Assert.*;

import org.junit.Test;

public class StudenteTest {

	@Test
	public void shouldsetNomeStudente() {
		// arrange
		Studente s = new Studente();
		// act
		s.setNome("Giovanni");
		// assert
		assertEquals("Giovanni", s.getNome());
	}

	@Test
	public void shouldsetCognomeStudente() {
		// arrange
		Studente s = new Studente();
		// act
		s.setCognome("Napoli");
		// assert
		assertEquals("Napoli", s.getCognome());
	}

	@Test
	public void shouldsetMatricolaStudente() {
		// arrange
		Studente s = new Studente();
		// act
		s.setMatricola(656473);
		// assert
		assertEquals(656473, s.getMatricola());
	}

	@Test
	public void shouldReturnNumeroEsami() {
		// arrange
		Studente s = new Studente("Giovanni", "Napoli", 656473);
		// act
		s.addEsame(new Esame("Test", 25, false, 9));
		// assert
		assertEquals(1, s.getNumeroEsami());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldBeOutOfBound() {
		// arrange
		Studente s = new Studente();
		// act
		s.getEsame(1);
		// assert
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldBeOutOfBound1() {
		// arrange
		Studente s = new Studente();
		// act
		s.getEsame(-1);
		// assert
	}

	@Test
	public void shouldReturnEsame() {
		// arrange
		Studente s = new Studente("Giovanni", "Napoli", 656473);
		// act
		s.addEsame(new Esame("Test", 25, false, 9));
		// assert
		assertEquals("Esame di Test (9 CFU) - voto: 25", s.getEsame(0).toString());
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldBeOutOfBound2() {
		// arrange
		Studente s = new Studente();
		// act
		s.eliminaEsame(1);
		// assert
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void shouldBeOutOfBound3() {
		// arrange
		Studente s = new Studente();
		// act
		s.eliminaEsame(-1);
		// assert
	}

	@Test
	public void shouldRemoveEsame() {
		// arrange
		Studente s = new Studente("Giovanni", "Napoli", 656473);
		// act
		s.addEsame("Test", 25, false, 9);
		s.eliminaEsame(0);
		// assert
		assertEquals(0, s.getListaEsami().size());
	}

	@Test
	public void shouldReturnMediapesata() {
		// arrange
		Studente s = new Studente("Giovanni", "Napoli", 656473);
		// act
		s.addEsame("Test", 25, false, 9);
		// assert
		assertEquals(25, s.getMediaPesata(), 0.1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void invalidArgumentMediaPesata() {
		// arrange
		Studente s = new Studente();
		// act
		s.getMediaPesata();
	}

	@Test
	public void shouldReturnMediaPartenza() {
		// arrange
		Studente s = new Studente("Giovanni", "Napoli", 656473);
		// act
		s.addEsame("Test", 30, false, 9);
		// assert
		assertEquals(110, s.getMediaPartenza(), 0.1);
	}

	@Test
	public void shouldReturnStudenteToString() {
		// arrange
		Studente s = new Studente("Giovanni", "Napoli", 656473);
		// act
		s.addEsame("Test", 30, false, 9);
		// assert
		assertEquals("Cognome: Napoli - Nome: Giovanni - Matricola: 656473", s.toString());
	}

	@Test
	public void shouldReturnStudenteToSaveString() {
		// arrange
		Studente s = new Studente("Giovanni", "Napoli", 656473);
		// act
		s.addEsame("Test", 30, false, 9);
		// assert
		assertEquals("Napoli , Giovanni , 656473", s.toSaveString());
	}

}
