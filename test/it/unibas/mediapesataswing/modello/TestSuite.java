package it.unibas.mediapesataswing.modello;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
   EsameTest.class,
   StudenteTest.class
})

public class TestSuite {   
}  	